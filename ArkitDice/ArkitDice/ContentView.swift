//
//  ContentView.swift
//  ArkitDice
//
//  Created by Arnaud Chrétien on 13/02/2022.
//

import SwiftUI
import ARKit
import RealityKit

struct ContentView: View {
    var body: some View {
        RealityKitView()
            .ignoresSafeArea()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
