//
//  ArkitDiceApp.swift
//  ArkitDice
//
//  Created by Arnaud Chrétien on 13/02/2022.
//

import SwiftUI

@main
struct ArkitDiceApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
